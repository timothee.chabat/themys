cmake_minimum_required(VERSION 3.8)
project(Themys)

find_package(ParaView REQUIRED)

if (NOT PARAVIEW_USE_QT)
  message(ERROR
    ": Please make sure ParaView has been built with PARAVIEW_USE_QT enabled.")
  return ()
endif()

if(NOT PARAVIEW_USE_PYTHON)
  message(ERROR
    ": Please make sure ParaView has been built with PARAVIEW_USE_PYTHON enabled.")
  return ()
endif()

include(GNUInstallDirs)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}")

add_subdirectory(plugins)
add_subdirectory(client)
