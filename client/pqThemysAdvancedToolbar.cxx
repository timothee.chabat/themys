#include "pqThemysAdvancedToolbar.h"

#include <QApplication>
#include <QLabel>
#include <QMessageBox>
#include <QStyle>

#include <pqActiveObjects.h>
#include <pqApplicationCore.h>
#include <pqCoreUtilities.h>
#include <pqPluginManager.h>
#include <pqServerManagerModel.h>
#include <pqSettings.h>
#include <pqUndoStack.h>
#include <vtkCommand.h>
#include <vtkSMProperty.h>
#include <vtkSMPropertyHelper.h>
#include <vtkSMProxyManager.h>
#include <vtkSMSessionProxyManager.h>
#include <vtkSMSettings.h>
#include <vtkSMSettingsProxy.h>

#include "pqThemysSettingsInfo.h"

//-----------------------------------------------------------------------------
class pqThemysAdvancedToolbar::vtkInternalObserver : public vtkCommand
{
public:
  static vtkInternalObserver* New() { return new vtkInternalObserver(); }

  void Execute(vtkObject* /*caller*/, unsigned long /*eventId*/, void* callData)
  {
    vtkSMProxyManager::RegisteredProxyInformation* info =
      static_cast<vtkSMProxyManager::RegisteredProxyInformation*>(callData);

    if (strcmp(info->GroupName, "settings") == 0 &&
      strcmp(info->ProxyName, SETTINGS_GROUP_NAME.toUtf8().data()) == 0)
    {
      self->setupConnections(vtkSMSettingsProxy::SafeDownCast(info->Proxy));
    }
  }

  pqThemysAdvancedToolbar* self = nullptr;
};

//-----------------------------------------------------------------------------
pqThemysAdvancedToolbar::pqThemysAdvancedToolbar(const QString& title, QWidget* parentW)
  : Superclass(title, parentW)
{
  this->constructor();
}

//-----------------------------------------------------------------------------
pqThemysAdvancedToolbar::pqThemysAdvancedToolbar(QWidget* parentW)
  : Superclass(parentW)
{
  this->setWindowTitle("Advanced");
  this->constructor();
}

//-----------------------------------------------------------------------------
void pqThemysAdvancedToolbar::constructor()
{
  this->AdvancedAction =
    this->addAction(QIcon(":/pqWidgets/Icons/pqAdvanced.svg"), "Advanced mode");
  this->AdvancedAction->setCheckable(true);

  // Every time the server is changed we add an observer to keep track of when the setting poxy is
  // added. When it is, the observer will call `setupConnections` so everything is in order for the
  // GUI. We use the `preServerAdded` because code path is
  //  - fire pqServerManagerModel::preServerAdded event
  //  - create all necessary proxies, including the pre-loaded plugins proxies
  //  - fire pqServerManagerModel::serverAdded and pqActiveObject::serverChanged events
  QObject::connect(pqApplicationCore::instance()->getServerManagerModel(),
    &pqServerManagerModel::preServerAdded, [this](pqServer* server) {
      if (server)
      {
        vtkNew<pqThemysAdvancedToolbar::vtkInternalObserver> observer;
        observer->self = this;

        // reset internal proxy so we do no try to call function on the proxy of the former server
        this->SettingsProxy = nullptr;
        server->proxyManager()->AddObserver(vtkCommand::RegisterEvent, observer);
      }
    });
}

//----------------------------------------------------------------------------
void pqThemysAdvancedToolbar::setupConnections(vtkSMSettingsProxy* settingsProxy)
{
  if (settingsProxy != nullptr && settingsProxy != this->SettingsProxy)
  {
    this->SettingsProxy = settingsProxy;

    // Connect on the property
    pqCoreUtilities::connect(this->SettingsProxy->GetProperty(ADVANCED_MODE_NAME.toUtf8().data()),
      vtkCommand::ModifiedEvent, this, SLOT(updateAdvancedMode()));

    // Connect the button
    this->connect(this->AdvancedAction, SIGNAL(triggered(bool)), SLOT(onTriggered(bool)));

    // Init the button state
    this->updateAdvancedMode();
  }
}

//-----------------------------------------------------------------------------
void pqThemysAdvancedToolbar::onTriggered(bool status)
{
  SCOPED_UNDO_EXCLUDE();
  if (!this->SettingsProxy)
  {
    return;
  }

  // Set the property
  vtkSMPropertyHelper(this->SettingsProxy, ADVANCED_MODE_NAME.toUtf8().data()).Set(status);
  this->SettingsProxy->UpdateVTKObjects();

  // Manually save it to user settings files
  pqSettings* qSettings = pqApplicationCore::instance()->settings();
  qSettings->saveInQSettings(
    QString(SETTINGS_GROUP_NAME + "." + ADVANCED_MODE_NAME).toUtf8().data(),
    this->SettingsProxy->GetProperty(ADVANCED_MODE_NAME.toUtf8().data()));
  vtkSMSettings* settings = vtkSMSettings::GetInstance();
  settings->SetSetting(
    QString(".settings." + SETTINGS_GROUP_NAME + "." + ADVANCED_MODE_NAME).toUtf8().data(), status);
}

//-----------------------------------------------------------------------------
void pqThemysAdvancedToolbar::updateAdvancedMode()
{
  if (!this->SettingsProxy)
  {
    return;
  }

  // Recover the property value
  bool use =
    vtkSMPropertyHelper(this->SettingsProxy, ADVANCED_MODE_NAME.toUtf8().data()).GetAsInt();

  // Update the button status
  this->AdvancedAction->setChecked(use);

  // Inform the application the setting has changed
  emit useAdvancedMode(use);
}
