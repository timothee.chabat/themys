#include <QToolBar>

class vtkSMSettingsProxy;

class pqThemysAdvancedToolbar : public QToolBar
{
  Q_OBJECT;
  using Superclass = QToolBar;

public:
  pqThemysAdvancedToolbar(const QString& title, QWidget* parent = nullptr);
  pqThemysAdvancedToolbar(QWidget* parent = nullptr);
  ~pqThemysAdvancedToolbar() override = default;

signals:

  /**
   * Signal emitted when the advanced mode is changed
   */
  void useAdvancedMode(bool status);

protected slots:
  /**
   * Slot called when the advanced mode setting is changed
   * to update the button state and emit the useAdvancedMode signal
   */
  void updateAdvancedMode();

  /**
   * Slot called when the advanced button is triggered
   */
  void onTriggered(bool status);

private:
  Q_DISABLE_COPY(pqThemysAdvancedToolbar);
  void constructor();

  QAction* AdvancedAction = nullptr;
  vtkSMSettingsProxy* SettingsProxy = nullptr;

  /**
   * Call to set up the signal/slot connection of the toolbar
   * The given proxy should be the proxy of the themysSettins plugin
   */
  void setupConnections(vtkSMSettingsProxy* settingsProxy);
  class vtkInternalObserver;
};
