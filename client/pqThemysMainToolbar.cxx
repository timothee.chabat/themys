#include "pqThemysMainToolbar.h"

#include "pqLoadDataReaction.h"
#include "pqUndoRedoReaction.h"

//-----------------------------------------------------------------------------
pqThemysMainToolbar::pqThemysMainToolbar(QWidget* parentW)
  : Superclass(parentW)
{
  this->setWindowTitle("Themys Controls");

  auto load = new QAction(QIcon(":/pqWidgets/Icons/pqOpen.svg"), "Open", this);
  this->addAction(load);
  new pqLoadDataReaction(load);

  auto undo = new QAction(QIcon(":/pqWidgets/Icons/pqUndo.svg"), "Undo", this);
  this->addAction(undo);
  new pqUndoRedoReaction(undo, true);
  auto redo = new QAction(QIcon(":/pqWidgets/Icons/pqRedo.svg"), "Redo", this);
  this->addAction(redo);
  new pqUndoRedoReaction(redo, false);
}
