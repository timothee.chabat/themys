#include <QToolBar>

class pqThemysMainToolbar : public QToolBar
{
  Q_OBJECT;
  using Superclass = QToolBar;

public:
  pqThemysMainToolbar(QWidget* parent);
  ~pqThemysMainToolbar() override = default;

private:
  Q_DISABLE_COPY(pqThemysMainToolbar);
};
