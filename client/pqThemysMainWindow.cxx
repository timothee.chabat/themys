#include "pqThemysMainWindow.h"
#include "ui_pqThemysMainWindow.h"

#include <pqApplicationCore.h>
#include <pqCameraUndoRedoReaction.h>
#include <pqDataQueryReaction.h>
#include <pqDeleteReaction.h>
#include <pqHelpReaction.h>
#include <pqInterfaceTracker.h>
#include <pqLoadDataReaction.h>
#include <pqLoadStateReaction.h>
#include <pqMacroReaction.h>
#include <pqManageLinksReaction.h>
#include <pqPVApplicationCore.h>
#include <pqParaViewBehaviors.h>
#include <pqParaViewMenuBuilders.h>
#include <pqPythonManager.h>
#include <pqPythonShell.h>
#include <pqRepresentation.h>
#include <pqSaveScreenshotReaction.h>
#include <pqSaveStateReaction.h>
#include <pqServerManagerModel.h>
#include <pqTraceReaction.h>
#include <pqUndoRedoReaction.h>
#include <vtkNew.h>
#include <vtkSMProperty.h>
#include <vtkSMProxy.h>

#include <QSet>

#include "pqThemysAdvancedToolbar.h"
#include "pqThemysMainToolbar.h"
#include "pqThemysSettingsReaction.h"
#include "pqThemysViewFrameActionsImplementation.h"

static const QSet<QString> ADVANCED_WIDGETS = { "Memory Inspector", "Output Messages",
  "Animation View", "Python Shell", "Main Controls", "Custom Viewpoints Toolbar",
  "Macros Toolbars" };

static const QString THEMYS_CATEGORY = "&Themys";

//-----------------------------------------------------------------------------
class pqThemysMainWindow::pqInternals : public Ui::pqClientMainWindow
{
public:
  QList<QToolBar*> ManagedToolbars;
  bool AdvancedMode = false;
};

//-----------------------------------------------------------------------------
pqThemysMainWindow::pqThemysMainWindow()
{
  // Setup default GUI layout.
  this->Internals = new pqInternals();
  this->Internals->setupUi(this);

  // Setup the python shell
  pqPythonShell* shell = new pqPythonShell(this);
  shell->setObjectName("pythonShell");
  this->Internals->pythonShellDock->setWidget(shell);

  // Setup the dock window corners to give the vertical docks more room.
  this->setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
  this->setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

  // Create a custom base file menu
  new pqLoadDataReaction(this->Internals->actionFileOpen);
  new pqLoadStateReaction(this->Internals->actionFileLoadState);
  new pqSaveStateReaction(this->Internals->actionFileSaveState);
  new pqSaveScreenshotReaction(this->Internals->actionFileSaveScreenshot);

  QObject::connect(this->Internals->actionExit, SIGNAL(triggered()), QApplication::instance(),
    SLOT(closeAllWindows()));

  // Create a custom base edit menu
  new pqUndoRedoReaction(this->Internals->actionEditUndo, true);
  new pqUndoRedoReaction(this->Internals->actionEditRedo, false);
  new pqCameraUndoRedoReaction(this->Internals->actionEditCameraUndo, true);
  new pqCameraUndoRedoReaction(this->Internals->actionEditCameraRedo, false);
  new pqDeleteReaction(this->Internals->actionEditResetSession, true);
  new pqDataQueryReaction(this->Internals->actionQuery);
  new pqThemysSettingsReaction(this->Internals->actionSettings);

  // Create a custom base tools menu
  new pqManageLinksReaction(this->Internals->actionToolsManageLinks);
  new pqTraceReaction(this->Internals->actionToolsStartStopTrace);

  // add macros menu in tools menu
  pqPythonManager* manager = pqPVApplicationCore::instance()->pythonManager();
  if (manager)
  {
    new pqMacroReaction(this->Internals->actionToolsImportMacro);
    manager->addWidgetForRunMacros(this->Internals->menuToolsBase);
    manager->addWidgetForEditMacros(this->Internals->menuToolsEditMacro);
    manager->addWidgetForDeleteMacros(this->Internals->menuToolsDeleteMacro);
  }

  // Populate application menus with actions.
  pqParaViewMenuBuilders::buildFileMenu(*this->Internals->menuFile);
  pqParaViewMenuBuilders::buildEditMenu(
    *this->Internals->menuEdit, this->Internals->propertiesPanel);

  // Populate sources menu.
  pqParaViewMenuBuilders::buildSourcesMenu(*this->Internals->menuSources, this);

  // Populate filters menu.
  pqParaViewMenuBuilders::buildFiltersMenu(*this->Internals->menuFilters, this);

  // Populate Tools menu.
  pqParaViewMenuBuilders::buildToolsMenu(*this->Internals->menuTools);

  // Populate View menu.
  pqParaViewMenuBuilders::buildViewMenu(*this->Internals->menuView, *this);

  // Setup the menu to show macros.
  pqParaViewMenuBuilders::buildMacrosMenu(*this->Internals->menuMacros);

  // Setup the help menu.
  pqParaViewMenuBuilders::buildHelpMenu(*this->Internals->menuHelp);

  // One time widget actions
  // Setup color editor
  // Provide access to the color-editor panel for the application and hide it
  pqApplicationCore::instance()->registerManager(
    "COLOR_EDITOR_PANEL", this->Internals->colorMapEditorDock);

  // Provide access to the find data panel for the application.
  pqApplicationCore::instance()->registerManager("FIND_DATA_PANEL", this->Internals->findDataDock);

  // Setup default GUI layout.
  this->setTabPosition(Qt::LeftDockWidgetArea, QTabWidget::North);

  // Set up the dock window corners to give the vertical docks more room.
  this->setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
  this->setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

  this->tabifyDockWidget(this->Internals->colorMapEditorDock, this->Internals->memoryInspectorDock);
  this->tabifyDockWidget(this->Internals->colorMapEditorDock, this->Internals->findDataDock);

  // Hide all other dockwidgets by default
  this->Internals->animationViewDock->hide();
  this->Internals->colorMapEditorDock->hide();
  this->Internals->findDataDock->hide();
  this->Internals->memoryInspectorDock->hide();
  this->Internals->multiBlockInspectorDock->hide();
  this->Internals->outputWidgetDock->hide();
  this->Internals->pythonShellDock->hide();

  this->tabifyDockWidget(this->Internals->animationViewDock, this->Internals->outputWidgetDock);
  this->tabifyDockWidget(this->Internals->animationViewDock, this->Internals->pythonShellDock);

  // setup properties dock
  this->tabifyDockWidget(this->Internals->propertiesDock, this->Internals->viewPropertiesDock);
  this->tabifyDockWidget(this->Internals->propertiesDock, this->Internals->displayPropertiesDock);
  this->tabifyDockWidget(this->Internals->propertiesDock, this->Internals->informationDock);
  this->tabifyDockWidget(this->Internals->propertiesDock, this->Internals->multiBlockInspectorDock);

  // Setup the context menu for the pipeline browser.
  pqParaViewMenuBuilders::buildPipelineBrowserContextMenu(
    *this->Internals->pipelineBrowser->contextMenu());

  pqParaViewMenuBuilders::buildToolbars(*this);
  this->addToolBarBreak();

  // Advanced mode
  pqThemysAdvancedToolbar* advToolbar = new pqThemysAdvancedToolbar(this);
  advToolbar->setObjectName("Advanced");
  advToolbar->layout()->setSpacing(0);
  this->addToolBar(advToolbar);
  this->connect(advToolbar, SIGNAL(useAdvancedMode(bool)), SLOT(useAdvancedMode(bool)));

  this->addToolBarBreak();
  auto mainToolbar = new pqThemysMainToolbar(this);
  mainToolbar->setObjectName("MainThemys");
  this->addToolBar(mainToolbar);

  // Enable help from the properties panel.
  QObject::connect(this->Internals->propertiesPanel,
    SIGNAL(helpRequested(const QString&, const QString&)), this,
    SLOT(showHelpForProxy(const QString&, const QString&)));

  /// Hook delete to pqDeleteReaction.
  QAction* tempDeleteAction = new QAction(this);
  pqDeleteReaction* handler = new pqDeleteReaction(tempDeleteAction);
  handler->connect(this->Internals->propertiesPanel, SIGNAL(deleteRequested(pqProxy*)),
    SLOT(deleteSource(pqProxy*)));

  // Define application behaviors.
  pqParaViewBehaviors::setEnableStandardViewFrameActions(true);
  new pqParaViewBehaviors(this, this);

  pqInterfaceTracker* pgm = pqApplicationCore::instance()->interfaceTracker();
  pgm->addInterface(new pqThemysViewFrameActionsImplementation(pgm));

  // Change interface for every  representations
  QObject::connect(pqApplicationCore::instance()->getServerManagerModel(),
    &pqServerManagerModel::representationAdded, [this](pqRepresentation* repr) {
      vtkSMProxy* proxy = repr->getProxy();
      if (proxy && strcmp(proxy->GetXMLGroup(), "representations") == 0)
      {
        vtkSMProperty* polarProp = proxy->GetProperty("PolarAxes");
        if (polarProp)
        {
          polarProp->SetPanelVisibility("default");
        }
        vtkSMProperty* specularProp = proxy->GetProperty("Specular");
        if (specularProp)
        {
          specularProp->SetPanelVisibility("advanced");
        }
      }
    });
}

//-----------------------------------------------------------------------------
pqThemysMainWindow::~pqThemysMainWindow()
{
  delete this->Internals;
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::useAdvancedMode(bool use)
{
  this->Internals->AdvancedMode = use;
  this->setupMenus(use);
  this->setupWidgets(use);
  this->setupToolbars(use);
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::setupMenus(bool advanced)
{
  this->Internals->menuFileBase->menuAction()->setVisible(!advanced);
  this->Internals->menuEditBase->menuAction()->setVisible(!advanced);
  this->Internals->menuToolsBase->menuAction()->setVisible(!advanced);
  this->Internals->menuFile->menuAction()->setVisible(advanced);
  this->Internals->menuEdit->menuAction()->setVisible(advanced);
  this->Internals->menuTools->menuAction()->setVisible(advanced);
  this->Internals->menuMacros->menuAction()->setVisible(advanced);
  this->Internals->menuHelp->menuAction()->setVisible(advanced);

  for (auto action : this->Internals->menuSources->actions())
  {
    action->setVisible(advanced || action->text() == THEMYS_CATEGORY);
  }
  for (auto action : this->Internals->menuFilters->actions())
  {
    action->setVisible(advanced || action->text() == THEMYS_CATEGORY);
  }
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::setupWidgets(bool advanced)
{
  if (!advanced)
  {
    QList<QDockWidget*> docks = this->findChildren<QDockWidget*>();
    for (auto dock : docks)
    {
      if (ADVANCED_WIDGETS.contains(dock->windowTitle()))
      {
        dock->hide();
      }
    }
  }
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::setupToolbars(bool advanced)
{
  QList<QToolBar*> toolbars = this->findChildren<QToolBar*>();
  for (auto toolbar : toolbars)
  {
    if (ADVANCED_WIDGETS.contains(toolbar->windowTitle()))
    {
      toolbar->setVisible(advanced);
    }
  }
}

//-----------------------------------------------------------------------------
QMenu* pqThemysMainWindow::createPopupMenu()
{
  QMenu* entries = this->Superclass::createPopupMenu();
  if (!this->Internals->AdvancedMode)
  {
    for (QAction* action : entries->actions())
    {
      if (ADVANCED_WIDGETS.contains(action->text()))
      {
        entries->removeAction(action);
      }
    }
  }
  return entries;
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::showHelpForProxy(const QString& groupname, const QString& proxyname)
{
#ifdef PARAVIEW_USE_QTHELP
  pqHelpReaction::showProxyHelp(groupname, proxyname);
#endif
}
