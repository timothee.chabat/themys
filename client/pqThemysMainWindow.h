/**
 * TODO
 */

#ifndef pqThemysMainWindow_h
#define pqThemysMainWindow_h

#include <QMainWindow>

class pqThemysMainWindow : public QMainWindow
{
  Q_OBJECT
  typedef QMainWindow Superclass;

public:
  pqThemysMainWindow();
  ~pqThemysMainWindow() override;

  /**
   * Overidden to only show basic entries when needed
   */
  QMenu* createPopupMenu() override;

public slots:
  /**
   * Slot called when the advanced mode should be used
   */
  void useAdvancedMode(bool use);

protected:
  /**
   * Hide/Show menus according to the advanced parameter
   */
  void setupMenus(bool advanced);

  /**
   * Hide/Show advanced toolbars according to the parameter
   */
  void setupToolbars(bool advanced);

  /**
   * Hide advanced dock widgets when advanced is false
   */
  void setupWidgets(bool advanced);

protected slots:

  /**
   * Show the help
   */
  void showHelpForProxy(const QString& groupname, const QString& proxyname);

private:
  Q_DISABLE_COPY(pqThemysMainWindow)

  class pqInternals;
  pqInternals* Internals;
};

#endif
