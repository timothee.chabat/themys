#include "pqThemysViewFrameActionsImplementation.h"

#include <vtkSMSettings.h>

#include "pqThemysSettingsInfo.h"

//-----------------------------------------------------------------------------
pqThemysViewFrameActionsImplementation::pqThemysViewFrameActionsImplementation(QObject* parent)
  : Superclass(parent)
{
}

//-----------------------------------------------------------------------------
QList<pqStandardViewFrameActionsImplementation::ViewType>
pqThemysViewFrameActionsImplementation::availableViewTypes()
{
  vtkSMSettings* settings = vtkSMSettings::GetInstance();
  bool advanced = settings->GetSettingAsInt(
    QString(".settings." + SETTINGS_GROUP_NAME + "." + ADVANCED_MODE_NAME).toUtf8().data(), false);

  if (advanced)
  {
    return this->Superclass::availableViewTypes();
  }
  else
  {
    pqStandardViewFrameActionsImplementation::ViewType info;
    info.Label = "3D View";
    info.Name = "RenderView";
    QList<pqStandardViewFrameActionsImplementation::ViewType> views{ info };
    return views;
  }
}
