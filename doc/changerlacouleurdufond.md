# Changer la couleur du fond

## Trouver la propriété

Sélectionner un objet (en cliquant dessus dans le "Pipeline Browser')".

Aller dans l'onglet "Properties", en dessous.

Ensuite, vous avez deux façons de procéder :

1. A la main

Passer en mode avancé en cliquant sur le bouton représentant une petite roue
au bout de la ligne d'édition de recherche par saisie du nom d'une propriété
("Search for properties by name").

Défilé en bas afin de trouver la propriété "Background".

2. En utilisant le mode "Search"

On peut aussi saisir dans cette ligne d'édition de recherche par saisie du
nom d'une propriété ("Search for properties by name") les premières lettres
de "Background" (sans les guillemets), par ex : Ba.

S'affiche alors que les propriétés commencant par ces premières lettres.

## Procéder au changement de la propriété

Différents modes sont présentés, le plus utilisé étant le "Single Color".

Le choix de la couleur se fait en cliquant sur le bouton "Color" juste
en-dessous afin dans un cercle la couleur actuellement sélectionnée.




