 - l'aspect composition d'objets pour ces objets 2D premier plan comme un Logo avec un PythonAnnotation afin de les déplacer ensemble
 
 Il est possible de lier la position d'un logo et d'un texte assez facilement.

 * Ouvrir ParaView
 * Source -> Texte
 * Entrer le texte Suivant : "               Text"
 * TextPosition -> LowerLeftCorner
 * Source -> Logo
 * Charger une image
 * Tools -> Manage Links -> Add -> Mode: PropertyLink -> TextSourceRepresentation:Position -> LogoSourceRepresentation:Position -> Ok
 * Logo and text are now moving together.

