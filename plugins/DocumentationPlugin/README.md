# Documentation plugin example

## Before building:

 - Install a MD to HTML converter (for example `pandoc`)
 - In the plugins/DocumentationPlugin/Ressources file, run `for f in *.md; do XXX; done`, where `XXX` is the command that will translate a single markdown file.
    - For pandoc, the command will be : `pandoc $f -f markdown -t html -s --metadata pagetitle="${f%.*}" -o ${f%.*}.html`
    - **Warning** HTML and MD cannot have the same name in the Ressources folder.
 - Once this is done, one can build themys as he usually do.

## PS

 - The `Dummy.xml` file is necessary in order to actually generate the documentation
 - One could improve this workflow using a CMake custom_command while creating some CMake variables so the user can set wich program will translate the MD files with which arguments. This workflow is not ideal either.
