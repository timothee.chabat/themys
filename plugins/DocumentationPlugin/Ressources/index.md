# Documentation plugin

This is an example using **markdown**. Below are *test* for supported features.

---

Here's an image :

![Sphere](sphere.jpg "My image title")

Here's a table :

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

Here's some quoting :

> Hello I'm quoting

Here's some code :

```
int main()
{
  I'm code;
}
```

Here's a list :

1. of one,
2. two,
  - two bis
  - two bisbis
3. three entry :)
